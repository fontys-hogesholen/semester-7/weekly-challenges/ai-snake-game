# Snake With Reinforcement Learning

## Implementation

The code base exists of the following aspects:
- The agent
- The game

### Agent

The agent handles making the decision in the game based on the current state of the game.

The agent implement the model which is an ANN implemented with pytorch.

### The game

This is the environment in which the agent is learning and performing its actions.

### Game Flow

The game starts by getting the current state of the game.

The agent performs an action in the game. After the move the reward, the new state are returned.

The old state, new statem, the reward and the action perfomed are passed to the agent inordert to train. This means to calculate the new Q value and further calculate the loss based on the Q value. The Q value indicates the quality of the action taken by the agent.

Considering the game has not reached the game over state the flow starts again. In the case the game has reached game over the model gets trained on the history of the previous games played and after the game start again.

## Reflection

After running the agent, it seems that the implmentation has one main shortcomming. Eventhough the model is cabale of leaning not to run into the wall or into itself, it has limited vision. The head of the snake can only look one "block" ahead (left, right, forward), this has the effect that the snake without knowing circles into itself because it can not see far enough.

The limitation of the model is not an issue in the early stage of the game, when the snake is short, rather this starts to become an issue when it reaches a length of roughly 30 "blocks".

Considering the amount of effort and time to implement this model it is still "impresive" to see a relatively good model be able to play snake on the same level an elementary kid would play.

## Resources

- [Snake Game](https://www.youtube.com/watch?v=--nsd2ZeYvs)
- [RL Snake Game](https://www.youtube.com/watch?v=L8ypSXwyBds)