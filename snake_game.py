import numpy as np
import pygame
import random

from enum import Enum
from collections import namedtuple


pygame.init()

font = pygame.font.Font("./fonts/arial.ttf", 25)


class Direction(Enum):
    RIGHT = 1
    LEFT = 2
    UP = 3
    DOWN = 4


Point = namedtuple("Point", "x, y")

# RGB values
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE1 = (0, 0, 255)
BLUE2 = (0, 100, 255)

BLOCK_SIZE = 20
SPEED = 10

class SnakeGame:

    def __init__(self, width: int = 640, height: int = 480) -> None:
        self.width = width
        self.height = height

        # initialize game window
        self.window = pygame.display.set_mode(size=(self.width, self.height))
        pygame.display.set_caption("Snake")
        self.clock = pygame.time.Clock()

        # initialize game state
        self.reset_game()

    def reset_game(self) -> None:
        self.direction = Direction.RIGHT

        self.head = Point(self.width / 2, self.height/2)
        self.snake = [
            self.head, 
            Point(self.head.x - BLOCK_SIZE, self.head.y), 
            Point(self.head.x - (2 * BLOCK_SIZE), self.head.y)
        ]

        self.score = 0
        self.food = None
        self._place_food()

        self.frame_iteration = 0

    def _place_food(self) -> None:
        """Private fuction that handles setting the food randomly in the game."""

        # handle picking a spot in the game
        x = random.randint(0, (self.width - BLOCK_SIZE) // BLOCK_SIZE) * BLOCK_SIZE
        y = random.randint(0, (self.height - BLOCK_SIZE) // BLOCK_SIZE) * BLOCK_SIZE

        # assign food point
        self.food = Point(x, y)

        # check that food in not in the same place as snake
        if self.food in self.snake:
            self._place_food()

    def _update_window(self) -> None:
        self.window.fill(BLACK)

        # draw snake on window
        for point in self.snake:
            pygame.draw.rect(
                self.window, 
                BLUE1, 
                pygame.Rect(point.x, point.y, BLOCK_SIZE, BLOCK_SIZE)
            )
            pygame.draw.rect(
                self.window, 
                BLUE2, 
                pygame.Rect(point.x + 4, point.y + 4, 12, 12)
            )
        # draw food
        pygame.draw.rect(
            self.window, 
            RED, 
            pygame.Rect(self.food.x, self.food.y, BLOCK_SIZE, BLOCK_SIZE)
        )

        # display score
        text = font.render(f"Score: {self.score}", True, WHITE)
        self.window.blit(text, [0, 0])  # show score at top left

        # render the changes
        pygame.display.flip()

    def _move(self, action):
        # [straight, right, left]

        clock_wise = [Direction.RIGHT, Direction.DOWN, Direction.LEFT, Direction.UP]
        index = clock_wise.index(self.direction)

        if np.array_equal(action, [1, 0, 0]):
            new_dir = clock_wise[index] # no change
        elif np.array_equal(action, [0, 1, 0]):
            next_index = (index + 1) % 4
            new_dir = clock_wise[next_index] # right turn r -> d -> l -> u
        if np.array_equal(action, [0, 0, 1]):
            next_index = (index - 1) % 4
            new_dir = clock_wise[next_index] # left turn r -> u -> l -> d

        self.direction = new_dir

        x = self.head.x
        y = self.head.y

        if self.direction == Direction.RIGHT:
            x += BLOCK_SIZE
        if self.direction == Direction.LEFT:
            x -= BLOCK_SIZE
        if self.direction == Direction.UP:
            y -= BLOCK_SIZE
        if self.direction == Direction.DOWN:
            y += BLOCK_SIZE

        self.head = Point(x, y)


    def is_collision(self, pt: Point = None) -> bool:
        if not pt:
            pt = self.head

        # checks if it hits edges
        if pt.x > self.width - BLOCK_SIZE or pt.x < 0 or pt.y > self.height - BLOCK_SIZE or pt.y < 0:
            return True
        # checks if it hits itself 
        # we are not interested in position 0 
        # that is the head which is alway in itself
        if pt in self.snake[1:]:
            return True

        return False

    def play_step(self, action):
        self.frame_iteration += 1
        
        # collect user input
        for event in pygame.event.get():

            # exit the game
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            
            # # check if key press has been registered
            # if event.type == pygame.KEYDOWN:
            #     # check which direction the user whants to move to
            #     if event.key == pygame.K_LEFT:
            #         self.direction = Direction.LEFT
            #     if event.key == pygame.K_RIGHT:
            #         self.direction = Direction.RIGHT
            #     if event.key == pygame.K_UP:
            #         self.direction = Direction.UP
            #     if event.key == pygame.K_DOWN:
            #         self.direction = Direction.DOWN
            
        # move snake
        self._move(action)
        self.snake.insert(0, self.head)

        # check if game over
        reward = 0
        game_over = False

        if self.is_collision() or self.frame_iteration > 100*len(self.snake):
            reward = -10
            game_over = True
            return reward, game_over, self.score

        # place new food if needed
        if self.head == self.food:
            self.score += 1
            reward = 10
            self._place_food()
        else:
            self.snake.pop()

        # update pygame window and clock
        self._update_window()
        self.clock.tick(SPEED)

        # return score and game over
        return reward, game_over, self.score


if __name__ == "__main__":
    game = SnakeGame()

    # game loop
    while True:
        reward, game_over, score = game.play_step()

        # break if game over
        if game_over:
            break

    pygame.quit()
